package com.ktyp.week6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int X_MAX = 19;
    public final static int Y_MIN = 0;
    public final static int Y_MAX = 19;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
        
    }
    public Robot(String name, char symbol) {
        this(name, symbol, 0, 0);
    }
    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }

    //Getter Setter Method
    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean up() {
        if(y==Y_MIN) return false;
        y = y-1;
        return true;
    }

    public boolean up(int step) {
        for(int i=0; i<step; i++) {
            if(!up()) {
                return false;
            }
        }
        return true;
    }

    public boolean down() {
        if(y==Y_MAX) return false;
        y =y+1;
        return true;
    }

    public boolean down(int step) {
        for(int i=0; i<step; i++) {
            if(!down()) {
                return false;
            }
        }
        return true;
    }

    public boolean left() {
        if(x==X_MIN) return false;
        x =x-1;
        return true;
    }

    public boolean left(int step) {
        for(int i=0; i<step; i++) {
            if(!left()) {
                return false;
            }
        }
        return true;
    }

    public boolean right() {
        if(x==X_MAX) return false;
        x = x+1;
        return true;
    }

    public boolean right(int step) {
        for(int i=0; i<step; i++) {
            if(!right()) {
                return false;
            }
        }
        return true;
    }
}
