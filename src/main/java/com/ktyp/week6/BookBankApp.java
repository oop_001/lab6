package com.ktyp.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank kittiyaporn = new BookBank("Kittiyaporn", 100.0);
        kittiyaporn.print();
        kittiyaporn.deposit(50); 
        kittiyaporn.print();
        kittiyaporn.withdraw(50);
        kittiyaporn.print();
        
        BookBank prayood = new BookBank("Prayood", 1);
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();
    }
}

